import { notifyBus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';

let data = { playbackVal: 0, audioDelayVal:0, subDelayVal: 0 }
let playbackElement;
let audioDelayElement;
let subDelayElement;

Vue.component('track-sync-modal', {
    template: '#track-sync-modal-template',
    data: function()
    {
        return data;
    },
    methods: {
        handleEvents: function()
        {
            data.playbackVal = playbackElement[0].value;

            data.audioDelayVal = audioDelayElement[0].value;
            
            data.subDelayVal = subDelayElement[0].value;
            
            playbackElement.on("input",function(e){
                data.playbackVal = e.currentTarget.value;  
            });
            
            audioDelayElement.on("input",function(e){
                data.audioDelayVal = e.currentTarget.value;  
            });

            subDelayElement.on("input",function(e){
                data.subDelayVal = e.currentTarget.value;  
            });
        }
    },
    mounted: function() {
        playbackElement = $('#playbackInput');
        audioDelayElement = $('#audioDelayInput');
        subDelayElement = $('#subDelayInput');
    	this.handleEvents();
    }
});