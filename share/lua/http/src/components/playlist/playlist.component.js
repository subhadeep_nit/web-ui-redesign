import { bus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';
import { player, playItem } from '../player/plyr.methods.js';

let playlistData;

Vue.component('playlist', {
    template: '#playlist-template',
    methods: {
        addItem: function(mode, id, title, src) {
            if(mode == 0)
            {
                this.$parent.playlistItems.push({ id: id, title: title, src: src });
            } 
            else if(mode == 1)
            {
                sendCommand(0,"command=in_enqueue&input="+src);
                this.clearPlaylist();
                bus.$emit('refreshPlaylist');
            }
        },
        removeItem: function(id) {
            sendCommand(0,"command=pl_delete&id="+id);
            this.$parent.playlistItems.splice({ id: id });
            sendCommand(1);
        },
        openPlaylist: function() {
            document.getElementById("playlistNav").style.width = "60%";
            document.getElementById("mobilePlaylistNavButton").style.width = "0%";
        },
        closePlaylist: function() {
            document.getElementById("playlistNav").style.width = "0%";
            document.getElementById("mobilePlaylistNavButton").style.width = "10%";
        },
        populatePlaylist: function(jData) {
            if(jData)
            {
                playlistData = jData;
            }
            if(playlistData)
            {
                this.$parent.playlistItems = [];
                for (var i = 0; i < playlistData[0].children[0].children.length; i++) {
                    this.addItem(0,playlistData[0].children[0].children[i].id, playlistData[0].children[0].children[i].name, playlistData[0].children[0].children[i].uri);
                }
            }
        },
        refreshPlaylist: function() {
            sendCommand(1);
            setInterval(function() {
                sendCommand(1);
            },5000);
        },
        clearPlaylist: function() {
            this.$parent.playlistItems = [];
        },
        play: function (src, id) {
            playItem(src,id);
        }
    },
    created: function() {
        bus.$on('openPlaylist', function() {
            this.openPlaylist();
        }.bind(this));

        bus.$on('closePlaylist', function() {
            this.closePlaylist();
        }.bind(this));

        bus.$on('populatePlaylist', function(jData) {
            this.populatePlaylist(jData);
        }.bind(this));

        bus.$on('addItem', function(params) {
            this.addItem(params[0], params[1], params[2], params[3]);
        }.bind(this));

        bus.$on('removeItem', function(id) {
            this.removeItem(id);
        }.bind(this));

        bus.$on('refreshPlaylist', function() {
            this.refreshPlaylist();
        }.bind(this));
        bus.$on('play', function(arg) {
            this.play(arg[0],arg[1]);
        }.bind(this));

        this.refreshPlaylist();
    }
});

$(document).click(function(e) {
    let container = $("#playlistNav");

    if($(window).width() <= 480 && !container.is(e.target) && container.has(e.target).length === 0 && container.css("width") != "0px"){
        bus.$emit('closePlaylist');
    }
});