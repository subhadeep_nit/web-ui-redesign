import { bus, notifyBus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';

let playlistItems;

Vue.component('playlist-buttons', {
    template: '#button-template',
    methods: {
    toggleRepeat: function ()
    {
        sendCommand(0,"command=pl_repeat");
    },
    startPlaylist: function()
    {
        playlistItems = this.$parent.$parent.$data.playlistItems;
        if(playlistItems[0])
        {
            notifyBus("play",playlistItems[0].src,playlistItems[0].id);
        }
    },
    toggleRandom: function()
    {
        sendCommand(0,"command=pl_random");
    }
},
    created: function()
    {
        bus.$on('toggleRepeat', function()
        {
            this.toggleRepeat();
        }.bind(this));

        bus.$on('startPlaylist', function() {
            this.startPlaylist();
        }.bind(this));

        bus.$on('toggleRandom', function() {
            this.toggleRandom();
        }.bind(this));
    }
});