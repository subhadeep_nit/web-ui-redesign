import { bus } from '../../services/bus.service.js';

Vue.component('sidenav', {
    template: '#sidenav-template',
    methods: {
    openNav: function () {
            if(screen.width <= 480){
                document.getElementById("sideNav").style.width = "60%";
            }
            else
            {
                document.getElementById("sideNav").style.width = "20%";
            }
        },
    closeNav: function () {
            document.getElementById("sideNav").style.width = "0";
        }
    },
    created: function()
    {
        bus.$on('openNav', function()
        {
            this.openNav();
        }.bind(this));

        bus.$on('closeNav', function()
        {
            this.closeNav();
        }.bind(this));
    }
});

