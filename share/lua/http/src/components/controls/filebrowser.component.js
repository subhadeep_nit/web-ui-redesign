import { notifyBus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';
import { video_types, audio_types, playlist_types } from '../../services/initialize.service.js';

Vue.component('file-modal', {
    template: '#file-modal-template',
    methods: {
    populateTree: function () {
    $("#file-tree").jstree({
        "core": {
          "mulitple": false, 
          "animation": 100,
          "check_callback" : true,
          "html_titles" : true,  
          "load_open" : true,
          "themes": {
            "variant": "medium",
            "dots": false
          },
          data: {
                url: function(node) {
                    if ( node.id == '#' ){
                        return "requests/browse.json?dir=/";
                    } else {
                        return "requests/browse.json?dir=/"+ node.data.path;
                    }
                },
                dataType : 'json',
                dataFilter: function(rawData) {
                    let data = JSON.parse(rawData);
                    let result = data.element.map(d => {
                        let res = {
                            text: d.name,
                            data: {
                                path: d.path,
                                uri: d.uri,
                                type: d.type
                            },
                            type: d.type,
                            children: true
                        };
                        if(d.type == "file")
                        {
                            res.children = false;
                        }
                        return res;
                    });
                    return JSON.stringify(result);
                }
            }
        },
        "types" : {
            "#" : {
              "valid_children" : ["root"]
            },
            "root" : {
              "icon" : "/static/3.3.4/assets/images/tree_icon.png",
              "valid_children" : ["default"]
            },
            "default" : {
              "valid_children" : ["default","file"]
            },
            "file" : {
              "icon" : "glyphicon glyphicon-file",
            }
        },
        "plugins" : [ "themes", "json_data", "ui", "cookies", "crrm", "sort", "types" ]
    });
    
    $('#file-tree').on('select_node.jstree', function (e, data) {
                node = data.instance.get_node(data.selected[0]);
                ext = (node.data.uri).substr(node.data.uri.lastIndexOf('.')+1).toLowerCase();
                if( node.data.type == "file" &&( $.inArray(ext, video_types) != -1||$.inArray(ext, audio_types) != -1|| $.inArray(ext, playlist_types) != -1) ){
                    notifyBus('addItem', [1,"",node.data.uri,node.data.uri]);
                }
            }).jstree();
        }
    },
    mounted: function()
    {
        this.populateTree();
    }
});
