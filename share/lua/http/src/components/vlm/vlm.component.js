import { bus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';

Vue.component('vlm-modal', {
    template: '#vlm-modal-template',
    methods: {
    executeVLM: function () {
            var cmd = document.getElementById("vlmCommand").value;
            console.log(cmd);
            sendCommand(2,"?command="+cmd);
            //console.log(xmlData);
        }
    },
    created: function()
    {
        bus.$on('executeVLM', function()
        {
            this.executeVLM();
        }.bind(this));
    }
});
