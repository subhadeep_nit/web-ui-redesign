import { sendCommand } from '../../services/command.service.js';

export let player;

export function plyrInit()
{
    player = plyr.setup({showPosterOnEnd:true});
    //setVideo('S6IP_6HG2QE','youtube');
    player[0].on('pause', function() {
        sendCommand(0,"command=pl_pause");     
    });

    player[0].on('play', function() {
        sendCommand(0,"command=pl_play");
    });

    player[0].on('volumechange', function(event) {
        var cmd = "command=volume&val="+event.detail.plyr.getVolume()*255;
        sendCommand(0,cmd);
    });
    
    /*player[0].on('loadstart', function(event) {
        //console.log(event.detail.plyr.getMedia());
    });*/
    return true;
}

export function setVideo(videoSrc, type, id)
{  
    player[0].source({
        type: 'video',
        title: '',
        sources: [{
        src: videoSrc,
        type: type
        }],
        poster: "assets/vlc.png"
    });
}

export function playItem(src, id)
{
    setVideo(src,'video/mp4', id);
    player[0].play();
    sendCommand(0,"command=pl_play&id="+id);
}