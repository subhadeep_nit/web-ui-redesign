import { notifyBus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';

Vue.component('stream-manager-modal', {
    template: '#stream-manager-modal-template',
    methods: {
    },
    created: function() {
    }
});