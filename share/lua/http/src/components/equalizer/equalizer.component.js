import { notifyBus } from '../../services/bus.service.js';
import { sendCommand } from '../../services/command.service.js';

let data = { eqVal: 0 }

Vue.component('equalizer-modal', {
    template: '#equalizer-modal-template',
    data:function() {
        return data;
    },
    methods: {
        handleEvents: function()
        {
            let equalizerElement = $('#equalizerInput');
            data.eqVal = equalizerElement[0].value; 
            //console.log(equalizerElement[0].value);
            equalizerElement.on("input",function(e){
                data.eqVal = e.currentTarget.value;  
                //sendCommand(0,"?command=setpreset&val="+e.currentTarget.value);
                /*sendCommand(3,{
                    command: ,
                    val: e.currentTarget.value,
                    band: "60Hz"
                });*/
               //console.log(e.currentTarget.value);
            });
        }
    },
    mounted: function() {
        this.handleEvents();
    }
});