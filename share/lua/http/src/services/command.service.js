import { bus } from './bus.service.js';

export let jsonData;
export let xmlData;
export let parseData;

export function sendCommand(mode, params) {
    if(mode == 0)
    {
        $.ajax({
            url: 'requests/status.json',
            data: params,
            success: function (data, status, jqXHR) {
                jsonData = data;
            }
        });
    } else if(mode == 1)
    {
        $.ajax({
            url: 'requests/playlist.json',
            data: params,
            success: function (data, status, jqXHR) {
                jsonData = JSON.parse(data);
                bus.$emit('populatePlaylist',[jsonData]);
            }
        });
    } else if(mode == 2)
    {
        $.ajax({
            url: 'requests/vlm_cmd.xml',
            data: params,
            success: function (data, status, jqXHR) {
                xmlData = data;
                console.log(xmlData);
            }
        });
    }
    /* else if(mode == 3)
    {
        
    } else if(mode == 4)
    {
        
    } else if(mode == 5)
    {
        
    }*/
}