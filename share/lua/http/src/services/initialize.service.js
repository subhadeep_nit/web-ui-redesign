import { plyrInit } from '../components/player/plyr.methods.js';
import { notifyBus } from './bus.service.js';

export let app;

export let video_types = [
           "asf", "avi", "bik", "bin", "divx", "drc", "dv", "f4v", "flv", "gxf", "iso",
           "m1v", "m2v", "m2t", "m2ts", "m4v", "mkv", "mov",
           "mp2", "mp4", "mpeg", "mpeg1",
           "mpeg2", "mpeg4", "mpg", "mts", "mtv", "mxf", "mxg", "nuv",
           "ogg", "ogm", "ogv", "ogx", "ps",
           "rec", "rm", "rmvb", "rpl", "thp", "ts", "txd", "vob", "wmv", "xesc" ];
export let audio_types = [
        "3ga", "a52", "aac", "ac3", "ape", "awb", "dts", "flac", "it",
        "m4a", "m4p", "mka", "mlp", "mod", "mp1", "mp2", "mp3",
        "oga", "ogg", "oma", "s3m", "spx", "thd", "tta",
        "wav", "wma", "wv", "xm"
];
export let playlist_types = [
        "asx", "b4s", "cue", "ifo", "m3u", "m3u8", "pls", "ram", "rar",
        "sdp", "vlc", "xspf", "zip", "conf"
];

$(function() {  // Handler for .ready() called.
	plyrInit();
    vueInit();

    $('#openNavButton').on('click',function (e){
        if($(window).width() <= 480 && $('#playlistNav').css("width") == "60%"){
            notifyBus("closePlaylist");
            notifyBus("openNav");
        } else {
            notifyBus("openNav");
        }
    });
    $('#closeNavButton').on('click',function (e){
        notifyBus("closeNav");
    });
    $('#vlmButton').on('click',function (e){
        notifyBus("executeVLM");
    });
    $('#repeatButton').on('click',function (e){
        notifyBus("toggleRepeat");
    });
    $('#playButton').on('click',function (e){
        notifyBus("startPlaylist");
    });
    $('#randomButton').on('click',function (e){
        notifyBus("toggleRandom");
    });
    $('#mobilePlaylistNavButton').on('click',function (e){
        if($(window).width() <= 480 && document.getElementById("sideNav").style.width == "60%"){
            notifyBus("closeNav");
            notifyBus("openPlaylist");
        } else {
            notifyBus("openPlaylist");
        }
    });
    /*$('#playlist').on('click',function (e){

    });*/
});

function vueInit()
{
    app = new Vue({
        el: '#app',
        data: {
            playlistItems: []
        }
    });
}